FROM golang:1.19.1-alpine3.16 as builder

ENV PKG_PATH=$GOPATH/src/rotate-ip

WORKDIR $PKG_PATH/
COPY . $PKG_PATH/

RUN go mod vendor

RUN go build main.go

ENTRYPOINT ["./main"]