package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/RezaAndriansyah/rotate-ip/models"
	"gitlab.com/RezaAndriansyah/rotate-ip/utils"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	v1 := r.Group("/v1")
	{

		v1.POST("/rotate/up", func(ctx *gin.Context) {

			var err error

			var payload models.RotateIpRequest
			body, _ := ioutil.ReadAll(ctx.Request.Body)
			json.Unmarshal(body, &payload)

			if err != nil {
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /rotate/up]",
						Data:    err,
					},
				)
				return
			}

			cmd := exec.Command("docker", "compose", "up", "-d", "--scale", fmt.Sprintf("proxy=%v", payload.TotalService))
			stdout, err := cmd.Output()

			if err != nil {
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /rotate/up]",
						Data:    err,
					},
				)
				return
			}

			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success Running rotate up in server [handler /rotate/up]",
					Data:    stdout,
				},
			)

		})

		v1.POST("/rotate/down", func(ctx *gin.Context) {

			var err error

			cmd := exec.Command("docker", "compose", "down")
			stdout, err := cmd.Output()

			if err != nil {
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /rotate/down]",
						Data:    err,
					},
				)
				return
			}

			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success Running rotate up in server [handler /rotate/down]",
					Data:    stdout,
				},
			)

		})

		v1.POST("/rotate/restart", func(ctx *gin.Context) {

			var err error

			cmd := exec.Command("docker", "compose", "restart")
			stdout, err := cmd.Output()

			if err != nil {
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /rotate/down]",
						Data:    err,
					},
				)
				return
			}

			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success Running rotate up in server [handler /rotate/down]",
					Data:    string(stdout),
				},
			)

		})

		v1.POST("/rotate/port", func(ctx *gin.Context) {

			var (
				err                   error
				splitResponseTerminal []string
				response              []string
				payload               models.RotateIpRequest
			)

			body, _ := ioutil.ReadAll(ctx.Request.Body)
			json.Unmarshal(body, &payload)

			if err != nil {
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: "Have error in server [handler /rotate/up]",
						Data:    err,
					},
				)
				return
			}

			cmd := exec.Command("curl", "ifconfig.me")
			ip, err := cmd.Output()

			if err != nil {
				ctx.JSON(
					http.StatusInternalServerError,
					utils.ResponseObject{
						Message: strings.Join([]string{"Have error in server [handler /rotate/port] => docker curl ", string(ip)}, ""),
						Data:    err,
					},
				)
				return
			}

			for i := 0; i < payload.TotalService; i++ {
				cmd := exec.Command("docker", "port", fmt.Sprintf("rotate-ip-proxy-%v", (i+1)), "8118/tcp")
				stdout, err := cmd.Output()

				if err != nil {
					ctx.JSON(
						http.StatusInternalServerError,
						utils.ResponseObject{
							Message: "Have error in server [handler /rotate/port] => docker port",
							Data:    err,
						},
					)
					return
				}

				splitResponseTerminal = strings.Split(string(stdout), "\n")
				splitResponseTerminal = strings.Split(splitResponseTerminal[0], ":")
				response = append(response, fmt.Sprintf("http://%v:%v", string(ip), splitResponseTerminal[1]))
			}

			ctx.JSON(
				http.StatusOK,
				utils.ResponseObject{
					Message: "Success Running rotate up in server [handler /rotate/down]",
					Data:    response,
				},
			)

		})

	}

	fmt.Println("Running in port 6666")
	r.Run(":6666")
}
